package com.odysseyinc.printtest;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.epson.eposprint.Builder;
import com.epson.eposprint.EposException;
import com.epson.eposprint.Print;
import com.epson.epsonio.DevType;
import com.epson.epsonio.DeviceInfo;
import com.epson.epsonio.EpsonIoException;
import com.epson.epsonio.Finder;
import com.epson.epsonio.IoStatus;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class BlueToothPrintActivity extends AppCompatActivity {

    Context mContext;

    final static int DISCOVERY_INTERVAL = 1;
    Button mConnectToPrinter;
    Button mBluetoothButton;
    private String openDeviceName = "192.168.192.168";
    private int connectionType = Print.DEVTYPE_BLUETOOTH;
    private int printerModel = Builder.LANG_EN;
    private String printerName = null;
    static final int SEND_TIMEOUT = 10 * 1000;
    private DeviceInfo[] mDeviceList;
    ScheduledExecutorService scheduler;
    ScheduledFuture<?> future;
    Runnable mRunnable;
    boolean mPrintable = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blue_tooth_print);
        mBluetoothButton = (Button) findViewById(R.id.print_button_bluetooth);
        mContext = this;

        Toast.makeText(mContext, "Starting search for printer", Toast.LENGTH_LONG).show();

        mBluetoothButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if(mPrintable)
                        Print();
                    else
                        Toast.makeText(mContext, "Printer not ready", Toast.LENGTH_LONG).show();
                } catch (EpsonIoException e) {
                    e.printStackTrace();
                }
            }
        });

        mRunnable = new Runnable() {
            @Override
            public void run() { //printer has been found execute runnable
                printerFound();
            }
        };

        // start find thread scheduler
        scheduler = Executors.newSingleThreadScheduledExecutor();
    }

    @Override
    public void onStart(){
        findPrintersTask();
        super.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            Finder.stop();
        } catch (EpsonIoException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_blue_tooth_print, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void getDevices() throws EpsonIoException {
        try {
            Finder.start(this, DevType.BLUETOOTH, null);
        } catch (EpsonIoException e) {
            //ShowMsg.showException(e, "getDevices", mContext);
            if (e.getStatus() == IoStatus.ERR_ILLEGAL) {
                Toast.makeText(this, String.valueOf("SEARCH ALREADY IN PROGRESS"), Toast.LENGTH_SHORT).show();
            } else if (e.getStatus() == IoStatus.ERR_PROCESSING) {
                Toast.makeText(this, String.valueOf("COULD NOT EXECUTE PROCESS"), Toast.LENGTH_SHORT).show();
            } else if (e.getStatus() == IoStatus.ERR_PARAM) {
                Toast.makeText(this, String.valueOf("INVALID PARAMETER PASSED"), Toast.LENGTH_SHORT).show();
            } else if (e.getStatus() == IoStatus.ERR_MEMORY) {
                Toast.makeText(this, String.valueOf("COULD NOT ALLOCATE MEMORY"), Toast.LENGTH_SHORT).show();
            } else if (e.getStatus() == IoStatus.ERR_FAILURE) {
                Toast.makeText(this, String.valueOf("UNSPECIFIED ERROR"), Toast.LENGTH_SHORT).show();
            }
        }
    }

    public Boolean checkForFoundPrinter() throws EpsonIoException{
         mDeviceList = Finder.getDeviceInfoList(Finder.FILTER_NONE);boolean isFound = false;

        int[] status = new int[1];

        if(mDeviceList.length>0){
            //Finder.stop();
        } else {
            Toast.makeText(getBaseContext(), "List is null", Toast.LENGTH_SHORT).show();
        }

        String deviceName = "";
        String printerName = "";
        int deviceType = 0;
        String macAddress = "";

        for(int i = 0; i < mDeviceList.length; i++){
            if( mDeviceList[i].getPrinterName() != null && mDeviceList[i].getPrinterName().equalsIgnoreCase("TM-T20II-m_000101")){
                deviceName = mDeviceList[i].getDeviceName();
                printerName = mDeviceList[i].getPrinterName();
                deviceType = mDeviceList[i].getDeviceType();
                macAddress = mDeviceList[i].getMacAddress();
                isFound = true;
            }
        }
        return isFound;
    }

    private void Print() throws EpsonIoException {
        mDeviceList = Finder.getDeviceInfoList(Finder.FILTER_NONE);

        int[] status = new int[1];

        if(mDeviceList.length>0){Finder.stop();}else{Toast.makeText(getBaseContext(), "List is null", Toast.LENGTH_SHORT).show();}

        String deviceName = "";
        String printerName = "";
        int deviceType = 0;
        String macAddress = "";

        for(int i = 0; i < mDeviceList.length; i++){
            if( mDeviceList[i].getPrinterName() != null && mDeviceList[i].getPrinterName().equalsIgnoreCase("TM-T20II-m_000101")){
                deviceName = mDeviceList[i].getDeviceName();
                printerName = mDeviceList[i].getPrinterName();
                deviceType = mDeviceList[i].getDeviceType();
                macAddress = mDeviceList[i].getMacAddress();
            }
        }
        Print printer = new Print(getApplicationContext());

        Log.i("Device Name: " + deviceName + "\n" + "Printer Name: " + printerName + "\n" + "Device Type: " + String.valueOf(deviceType) + "\n" + "MAC: " + macAddress, "");

        try {

            //Print Data Builder
            Builder builder = new Builder("TM-T20", Builder.MODEL_ANK);

            // Section 1 : Store information
            builder.addFeedLine(1);

            // Text buffer
            StringBuilder textData = new StringBuilder();

            // addBarcode API settings
            final int barcodeWidth = 2;
            final int barcodeHeight = 100;

            textData.append("THE STORE 1234 (555) 555-5555\n");
            textData.append("STORE DIRECTOR - John Smith\n");
            textData.append("\n");
            textData.append("7/01/07 16:58 6153 05 0191 134\n");
            textData.append("ST# 21 OP# 001 TE# 01 TR# 747\n");
            textData.append("------------------------------\n");

            builder.addText(textData.toString());
            textData.delete(0, textData.length());

            // Section 2 : Purchased items
            textData.append("004 OHEIDA 3PK SPRINGF  9.99 R\n");
            textData.append("104 3 CUP BLK TEAPOT    9.99 R\n");
            textData.append("451 EMERIL GRIDDLE/PAN 17.99 R\n");
            textData.append("389 CANDYMAKER ASSORT   4.99 R\n");
            textData.append("740 TRIPOD              8.99 R\n");
            textData.append("334 BLK LOGO PRNTED ZO  7.99 R\n");
            textData.append("581 AQUA MICROTERRY SC  6.99 R\n");
            textData.append("934 30L BLK FF DRESS   16.99 R\n");
            textData.append("075 LEVITATING DESKTOP  7.99 R\n");
            textData.append("412 **Blue Overprint P  2.99 R\n");
            textData.append("762 REPOSE 4PCPM CHOC   5.49 R\n");
            textData.append("613 WESTGATE BLACK 25  59.99 R\n");
            textData.append("------------------------------\n");

            builder.addText(textData.toString());
            textData.delete(0, textData.length());

            // Section 3 : Payment information
            textData.append("SUBTOTAL                160.38\n");
            textData.append("TAX                      14.43\n");

            builder.addText(textData.toString());
            textData.delete(0, textData.length());

            builder.addTextDouble(Builder.TRUE, Builder.TRUE);
            builder.addText("TOTAL    174.81\n");
            builder.addTextDouble(Builder.FALSE, Builder.FALSE);
            builder.addFeedLine(1);

            textData.append("CASH                    200.00\n");
            textData.append("CHANGE                   25.19\n");
            textData.append("------------------------------\n");

            builder.addText(textData.toString());
            textData.delete(0, textData.length());

            // Section 4 : Advertisement
            textData.append("TotalNumber of Items Purchased\n");
            textData.append("Sign Up and Save !\n");
            textData.append("With Preferred Saving Card\n");

            builder.addText(textData.toString());
            textData.delete(0, textData.length());

            builder.addFeedLine(2);

            // Add barcode data to command buffer
            builder.addBarcode("01209457",
                    Builder.BARCODE_CODE39,
                    Builder.HRI_BELOW,
                    Builder.FONT_A,
                    barcodeWidth,
                    barcodeHeight);

            // Add command to cut receipt to command buffer
            builder.addCut(Builder.CUT_FEED);


            builder.addCut(Builder.CUT_FEED);

            if(builder!=null) {
                Log.i("BUILDER NOT NULL", "");
            }

            //Printer Test Builder
            Builder confirmBuilder = new Builder("TM-T20", Builder.MODEL_ANK);

            //Open printer
            printer.openPrinter(Print.DEVTYPE_BLUETOOTH, deviceName);

            //Send Test Builder
            printer.sendData(confirmBuilder, SEND_TIMEOUT, status);

            //Check printer Status
            if((status[0] & Print.ST_OFF_LINE) != Print.ST_OFF_LINE) {
                //If online send print data
                Log.i("PRINTER NOT OFFLINE", "");
                printer.sendData(builder, SEND_TIMEOUT, status);

                //Check if data sent successfully
                if((status[0] & Print.ST_PRINT_SUCCESS) == Print.ST_PRINT_SUCCESS) {
                    builder.clearCommandBuffer();
                    Toast.makeText(this, "DATA SENT SUCCESSFULLY", Toast.LENGTH_SHORT).show();
                }
                printer.closePrinter();
            } else if ((status[0] & Print.ST_OFF_LINE) == Print.ST_OFF_LINE) {
                Toast.makeText(this, "PRINTER OFFLINE", Toast.LENGTH_SHORT).show();
            }else{
                Toast.makeText(this, "OTHER PRINTER ERROR", Toast.LENGTH_SHORT).show();
            }
        } catch (EposException e) {
            e.printStackTrace();
            //ShowMsg.showException(e,"Print", mContext);
        }
    }
    public void findPrintersTask(){
        try {
            getDevices();
        } catch (EpsonIoException e) {
            e.printStackTrace();
        }
        future = scheduler.scheduleWithFixedDelay(mRunnable, 3, DISCOVERY_INTERVAL, TimeUnit.SECONDS);
    }

    public void printerFound() {
        try {
            mPrintable = checkForFoundPrinter();
            if(mPrintable){
                BlueToothPrintActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(mContext, "Printer found ready to print", Toast.LENGTH_LONG).show();

                    }
                });
            }
            else {
                BlueToothPrintActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(mContext, "Printer not found searching again", Toast.LENGTH_LONG).show();
                    }
                });
            }
        } catch (EpsonIoException e) {
            e.printStackTrace();
        }
    }
}
